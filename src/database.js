import firebase from 'firebase'
import "firebase/database"

const config = {
    apiKey: "AIzaSyCy40uFvdB3tm5f_mN4noPaHBrLoMdnAaY",
    authDomain: "leisure-dredger.firebaseapp.com",
    projectId: "leisure-dredger",
    storageBucket: "leisure-dredger.appspot.com",
    messagingSenderId: "484519909443",
    appId: "1:484519909443:web:c818f1b3190819fc3e9cf6"
} //API_KEYS (nos la proporciona firebase)

const database = firebase.initializeApp(config)

export default database
