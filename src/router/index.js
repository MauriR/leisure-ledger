import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Home'
import Register from '../views/Register'
import Create from '../views/CreateLedger'
import Menu from '../views/Menu'
import Temporal from '../views/Temporal'

const routes = [
  // {
  //   path:'/lazy',
  //   name:'Lazy',
  //   component:()=>import ('../views/Lazy.vue')
  // } Ejemplo de lazy loading de una ruta. 
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/register',
    name: 'Register',
    component: Register
  },
  {
    path: '/login',
    name: 'Login',
    component: ()=>import('../views/Login.vue')
  },

  {
    path: '/menu',
    name: 'Menu',
    component: Menu
  },

  {
    path: '/create',
    name: 'Create',
    component: Create
  },

  {
    path: '/temporal',
    name: 'Temporal',
    component: Temporal
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  //history: createWebHistory(),
  routes
})

export default router
